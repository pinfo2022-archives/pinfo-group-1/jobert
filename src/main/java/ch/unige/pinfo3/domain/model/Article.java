package ch.unige.pinfo3.domain.model;

import lombok.ToString;

@ToString
public class Article {
    public String Title;
    public String Published;
    public String DOI;
    public String PmcId;
    public String Authors; 
    public String Abstract;
    public String Url;
    public String Journal; 
    public String labels;
    public int cluster;
    public double x;
    public double y;
}