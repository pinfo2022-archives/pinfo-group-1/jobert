package ch.unige.pinfo3.domain.model;

public record Job(String query, String corpus, int nTrials, int maxEntries) {
}
