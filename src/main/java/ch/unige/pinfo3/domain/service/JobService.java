package ch.unige.pinfo3.domain.service;

import ch.unige.pinfo3.domain.model.Job;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.quarkus.logging.Log;

import io.fabric8.kubernetes.api.model.EnvVar;
import io.fabric8.kubernetes.api.model.batch.v1.JobBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.apache.commons.lang3.StringUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class JobService {
    @ConfigProperty(name = "kafka.bootstrap.servers")
    String kafkaServer;
    @ConfigProperty(name = "kubernetes.registry.secret-name")
    String secretName;
    @ConfigProperty(name = "kubernetes.registry.secret")
    boolean secret;

    @Inject
    KubernetesClient client;

    public void createJob(Job job) {
        final var jobName = StringUtils.left(
                "pullclust-" + job.query()
                        .trim()
                        .toLowerCase()
                        .replaceAll("\\W", ""),
                63);

        var jobBuilder = new JobBuilder()
                .withApiVersion("batch/v1")
                .withNewMetadata()
                .withName(jobName)
                .endMetadata()
                .withNewSpec()
                .withNewTemplate()
                .withNewSpec()
                .addNewContainer()
                .withName("puller-clusterer")
                .withImage("registry.gitlab.unige.ch/pinfo-2022/pinfo-3/puller-clusterer:latest")
                .withCommand("/entrypoint.sh")
                .withArgs(job.query(),
                        job.corpus(),
                        Integer.toString(job.nTrials()),
                        Integer.toString(job.maxEntries()))
                .withEnv(new EnvVar("KAFKA_SERVER", kafkaServer, null))
                .endContainer()
                .withRestartPolicy("Never")
                .endSpec()
                .endTemplate()
                .withBackoffLimit(0)
                .withTtlSecondsAfterFinished(100)
                .endSpec();

        if (secret) {
            jobBuilder = jobBuilder
                    .editSpec()
                    .editTemplate()
                    .editSpec()
                    .addNewImagePullSecret(secretName)
                    .endSpec()
                    .endTemplate()
                    .endSpec();
        }

        try {
            Log.info("Creating puller-clusterer job");
            final var k8sJob = jobBuilder.build();
            client.batch().v1().jobs().createOrReplace(k8sJob);
        } catch (KubernetesClientException e) {
            Log.error("Unable to create puller-clusterer job");
            throw e;
        }
    }
}
