package ch.unige.pinfo3.domain.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import ch.unige.pinfo3.api.msg.ArticleProducer;
import ch.unige.pinfo3.domain.model.Article;
import io.quarkus.logging.Log;

@ApplicationScoped
public class ArticleService {
    @Inject
    ArticleProducer ap;

    public void submit(String ucnf, Article article) {
        Log.debug("Received Pyrticle with Title = " + article.Title);
        if (isValid(article)) {
            ap.send(ucnf, article);
        }
    }

    public Boolean isValid(Article article) {
        return true;
    }
}
