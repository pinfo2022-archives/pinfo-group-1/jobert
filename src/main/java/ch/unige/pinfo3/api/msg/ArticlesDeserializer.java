package ch.unige.pinfo3.api.msg;

import ch.unige.pinfo3.domain.model.Article;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class ArticlesDeserializer extends ObjectMapperDeserializer<Article> {
    public ArticlesDeserializer() {
        super(Article.class);
    }
}
