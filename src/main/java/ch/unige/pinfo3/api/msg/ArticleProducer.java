package ch.unige.pinfo3.api.msg;

import java.util.concurrent.CompletionStage;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import ch.unige.pinfo3.domain.model.Article;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import io.smallrye.reactive.messaging.kafka.Record;

@ApplicationScoped
public class ArticleProducer {
    @Inject
    @Channel("Jarticles")
    Emitter<Record<String, Article>> articleEmmiter;

    public CompletionStage<Void> send(String ucnf, Article article) {
        var rec = Record.of(ucnf, article);
        return articleEmmiter.send(rec);
    }
}
