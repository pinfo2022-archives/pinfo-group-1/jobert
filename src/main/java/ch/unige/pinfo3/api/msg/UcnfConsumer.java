package ch.unige.pinfo3.api.msg;

import ch.unige.pinfo3.domain.service.JobService;
import ch.unige.pinfo3.domain.model.Job;
import io.quarkus.logging.Log;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.inject.Inject;

public class UcnfConsumer {
    @Inject
    JobService jobService;

    @ConfigProperty(name = "sysreview.corpus", defaultValue = "Abstract")
    String corpus;

    @ConfigProperty(name = "sysreview.trials", defaultValue = "4")
    int nTrials;

    @ConfigProperty(name = "sysreview.max-entries", defaultValue = "500")
    int maxEntries;

    @Inject
    JobService js;

    @Incoming("ucnfs")
    public void consume(String ucnf) {
        Log.info(String.format("Received ucnf '%s'", ucnf));
        js.createJob(new Job(ucnf, corpus, nTrials, maxEntries));
    }
}
