package ch.unige.pinfo3.api.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import ch.unige.pinfo3.domain.model.Article;
import ch.unige.pinfo3.domain.service.ArticleService;

@ApplicationScoped
public class ArticleConsumer {
    @Inject
    ArticleService as;

    @Incoming("Pyrticles")
    public void consume(ConsumerRecord<String, Article> record) {
        final var ucnf = record.key();
        final var article = record.value();
        as.submit(ucnf, article);
    }
}
