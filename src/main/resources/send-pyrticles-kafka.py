import json
from kafka import KafkaProducer

if __name__ == '__main__':
    producer = KafkaProducer(bootstrap_servers='localhost:9092', max_request_size=100_000_000)

    with open('Pyrticles.json', "rb") as f:
        articles = json.load(f)
        for article in articles:
            producer.send("Jarticles", json.dumps(article).encode(), "HIV".encode())
