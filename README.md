# Jobert - The job spawner

This tool aims to spawn Kubernetes Jobs to run clusterings for a given ucnf query.
Jobert will spawn a new Job for each new query, the Job will contain the Puller/Clusterer
microservice which will pull the articles from ArXiv and run a clustering on them. The results
are then communicated back to Jobert through Kafka.

## Workflow

1. Jobert receive a new search query.
2. Jobert creates a new K8S Job.
3. Jobert receives Job results via a stream or Articles and ucnf pairs.
4. Jobert does filtering out of invalid articles and weird articles and sends the result to backend via Kafka.

## Kafka Channels

Communication is done over the following Kafka channels:

Backend ------------ucnfs-----------> Jobert

         ** Jobert Spawns K8S Job ** 

Jobert  <----------Pyrticles--------- Python MicroService

Backend <----------Jarticles--------- Jobert

## Technical Details

Jobert will use Kubernetes to spawn pods using [Jobs](https://kubernetes.io/docs/concepts/workloads/controllers/job/)
for each new request sent by the client. Each pod will contain its own python installation and will communicate to a
centralized PostgreSQL database to retrieve articles.

For each request, a new Job is defined and spawns a pod that is dedicated to download any new article using online
databases (such as pubmed). It will fill the Postgres DB with any new article found on the article database. Once the
article retrieval is done, it will start the clustering. Once the clustering is done, the pod will kill itself and the
Job can be considerate as done.

## Messaging specs

### Topic _ucnfs_

Jobert listen on the topic `ucnfs`. A message on `ucnfs` must be a string (or a UTF-8 encoded bytes array) which is the query that will be used to create a new Job.

### Topic _Pyrticles_

Jobert listen on the topic `Pyrticles`, which is the message topic used by PullerClusterer to send results when a clustering is done. When a job is done, it will send each article from the request to the topic.

The record follows this scheme:

#### Key

`ucnf`

#### Message

| Field     | type                                                 |
| --------- | ---------------------------------------------------- |
| Title     | _string_                                             |
| Published | _date string_                                        |
| DOI       | _string or null_                                     |
| PmcId     | _string or null_                                     |
| Authors   | _Array of strings, format "[author1, author2, ...]"_ |
| Abstract  | _string_                                             |
| Url       | _string_                                             |
| Journal   | _string or null_                                     |
| Fulltext  | _string or null_                                     |
| labels    | _string_                                             |
| text      | _Array of string_                                    |
| cluster   | _int_                                                |
| x         | _float_                                              |
| y         | _float_                                              |

The message is encoded with [JSON](https://www.json.org).

# Jobert Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:

```shell script
./gradlew quarkusDev
```
